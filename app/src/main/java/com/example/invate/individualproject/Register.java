package com.example.invate.individualproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity implements View.OnClickListener {

    Button Register;
    EditText Username, Password, Email, ID;

    public static final String usernameKey = "usernameKey";
    public static final String passwordKey = "passwordKey";
    public static final String emailKey = "emailKey";
    public static final String idKey = "idKey";

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Username = (EditText) findViewById(R.id.usernameText);
        Password = (EditText) findViewById(R.id.passwordText);
        Email = (EditText) findViewById(R.id.emailText);
        ID = (EditText) findViewById(R.id.IDText);

        sharedPreferences = getSharedPreferences("MySharedPreMain", Context.MODE_PRIVATE);

        if (sharedPreferences.contains(usernameKey)){
            Username.setText(sharedPreferences.getString(usernameKey, ""));
        }

        if (sharedPreferences.contains(passwordKey)){
            Password.setText(sharedPreferences.getString(passwordKey, ""));
        }

        if (sharedPreferences.contains(emailKey)){
            Email.setText(sharedPreferences.getString(emailKey, ""));
        }

        if (sharedPreferences.contains(idKey)){
            ID.setText(sharedPreferences.getString(idKey, ""));
        }

        Register = (Button) findViewById(R.id.register);

        Register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register:
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(usernameKey, Username.getText().toString());
                editor.putString(passwordKey, Password.getText().toString());
                editor.putString(emailKey, Email.getText().toString());
                editor.putString(idKey, ID.getText().toString());
                editor.commit();
                Toast.makeText(this, "Data Registered", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(Register.this, DoctorActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("Key1", "this is the message");
                startActivity(intent);

                break;
        }
    }
}
