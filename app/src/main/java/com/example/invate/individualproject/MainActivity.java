package com.example.invate.individualproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


//extends AppCompactivity is an inheritance feature that is used to navigate down the hierarchy
public class MainActivity extends AppCompatActivity {
    // Creating the button variables labelled doctor and patient
    Button patientButton;
    Button doctorButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //use this command for opening a certain interface
        setContentView(R.layout.activity_main);
        //An identifier must be declared for the button to work
        patientButton = (Button) findViewById(R.id.button3);
        patientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Displays the string when a patient button is clicked
                Toast.makeText(getBaseContext(), "Patient Button Clicked" , Toast.LENGTH_SHORT ).show();
                //When clicked this will open up the patient activity interface
                Intent intent = new Intent(MainActivity.this, PatientActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        doctorButton = (Button) findViewById(R.id.button4);
        doctorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Doctor Button Clicked!" , Toast.LENGTH_SHORT ).show();
                //When clicked this will open up the doctor activity interface
                Intent intent = new Intent(MainActivity.this, DoctorActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }}







/*
        public void doctorButton(View v){
            Intent intent=new Intent(MainActivity.this, DoctorActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("Key 1", "Doctor Button Clicked");
            startActivity(intent);
        }

        public void patientButton(View v){
                Intent intent = new Intent(MainActivity.this, PatientActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("Key 1", "Patient Button Clicked");
                startActivity(intent);
            }
        }
*/


