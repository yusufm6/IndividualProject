package com.example.invate.individualproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Invate on 20/03/2017.
 */

public class DoctorActivity extends AppCompatActivity implements View.OnClickListener {

    Button Login;
    EditText Username, Password, Email, ID;
    Button Signup;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor);
        Username = (EditText) findViewById(R.id.usernameText);
        Password = (EditText) findViewById(R.id.passwordText);
        Email = (EditText) findViewById(R.id.emailText);
        ID = (EditText) findViewById(R.id.IDText);
        Login = (Button) findViewById(R.id.Login);
        Signup = (Button) findViewById(R.id.Signup);

        Login.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Login:

                startActivity(new Intent(DoctorActivity.this, DoctorMenuActivity.class));



                break;
            case R.id.Signup:

                startActivity(new Intent(DoctorActivity.this, Register.class));

                break;


        }
    }

    public void LoginButton(View v){

        Intent intent=new Intent(DoctorActivity.this, DoctorMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);

    }

    public void CancelButton(View v){

        Intent intent=new Intent(DoctorActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);

    }

    public void RegisterButton(View v){

        Intent intent=new Intent(DoctorActivity.this, Register.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);

    }

    }

