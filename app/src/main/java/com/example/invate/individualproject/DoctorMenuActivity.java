package com.example.invate.individualproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Invate on 20/03/2017.
 */

public class DoctorMenuActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_menu);
    }

    //This button will direct the Doctor to view the appointments available
    public void viewAppointmentButton(View v){
        Intent intent=new Intent(DoctorMenuActivity.this, ViewAppointment.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    //This button will direct the Doctor to view the machine learning model
    public void viewModelButton(View v){
        Intent intent=new Intent(DoctorMenuActivity.this, MachineLearningModel.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    // The doctor can view report when the view report button is clicked
    public void viewReportButton(View v){
        Intent intent=new Intent(DoctorMenuActivity.this, PatientReportActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}

