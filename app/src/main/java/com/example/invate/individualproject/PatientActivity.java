package com.example.invate.individualproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Invate on 20/03/2017.
 */

public class PatientActivity extends AppCompatActivity {
    //Declaring EditText Variables
    EditText name;
    EditText age;

    //Key-value pairs each key-value pair is a constant(remember to add these in xml)
    public static final String nameKey = "nameKey";
    public static final String ageKey = "ageKey";


    //used for modifying and accesing preference data
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient);
        //idenitfying the variables via XML
        name = (EditText) findViewById(R.id.nameText);
        age = (EditText) findViewById(R.id.ageText);
        sharedPreferences = getSharedPreferences("MySharedPreMain", Context.MODE_PRIVATE);

        //The name and age must contain some input for their values to be saved
        if (sharedPreferences.contains(nameKey)){
            name.setText(sharedPreferences.getString(nameKey, ""));
        }

        if (sharedPreferences.contains(ageKey)){
            age.setText(sharedPreferences.getString(ageKey, ""));
        }


    }


    public void SaveButton(View v){
        //Values vill be saved and edited when the button is clicked
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(nameKey, name.getText().toString());
        editor.putString(ageKey, age.getText().toString());
        //The commit command is needed to save the data
        editor.commit();
        //To let the user know data has been saved
        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show();
        //This will open the menu screen for the patient
        Intent intent=new Intent(PatientActivity.this, PatientMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);



    }


}
