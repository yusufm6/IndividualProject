package com.example.invate.individualproject;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Invate on 06/04/2017.
 */

public class PatientReportActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_report);

        TextView textView1 = (TextView)findViewById(R.id.textView8);

        //Creating a database for the patient report
        SQLiteDatabase report = openOrCreateDatabase("reportdb.db", MODE_PRIVATE, null);


        //SQL commands and queries
        report.execSQL("create table if not exists ReportTable(ID, Result)");

        report.execSQL("insert into ReportTable values('55', 'malignant'");

        //Will view all the data from the report table when the curser is moved to the textview
        Cursor cursor = report.rawQuery("select * from ReportTable", null);

        cursor.moveToFirst();

        //The variables are declared as strings and will display via the the command select *
        String ID = cursor.getString(0);
        String Result = cursor.getString(1);

        textView1.setText(ID+ Result + "\n");
        report.close();
    }

}
