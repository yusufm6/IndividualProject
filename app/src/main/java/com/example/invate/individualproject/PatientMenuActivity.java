package com.example.invate.individualproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Invate on 20/03/2017.
 */

public class PatientMenuActivity extends AppCompatActivity{

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_menu);
    }

    public void cancerInformationButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, CancerInformation.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void cancerSymptomsButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, CancerSymptoms.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void runTestButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, RunTest.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    public void viewTestButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, ViewTest.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void makeAppointmentButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, ViewAppointment.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void questionButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, Questions.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void menuButton(View v){
        Intent intent=new Intent(PatientMenuActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
