package com.example.invate.individualproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Invate on 06/04/2017.
 */

public class CancerInformation extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancer_information);
    }

    public void breastCancerInformationButton(View v){
        Intent intent=new Intent(CancerInformation.this, BreastCancerInformation.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void lungCancerInformationButton(View v){
        Intent intent=new Intent(CancerInformation.this, LungCancerInformation.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
