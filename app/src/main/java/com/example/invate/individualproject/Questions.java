package com.example.invate.individualproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Invate on 06/04/2017.
 */

public class Questions extends AppCompatActivity{

    EditText question;

    public static final String questionKey = "questionKey";

    SharedPreferences sharedPreferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questions);
        question = (EditText) findViewById(R.id.questionText);
        sharedPreferences = getSharedPreferences("MySharedPreMain", Context.MODE_PRIVATE);


        if (sharedPreferences.contains(questionKey)) {
            question.setText(sharedPreferences.getString(questionKey, ""));
        }
    }

    public void saveQuestionButton(View v){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(questionKey, question.getText().toString());
        editor.commit();
        Toast.makeText(this, "Questions Saved", Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(Questions.this, PatientMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Key1", "this is the message");
        startActivity(intent);

    }


}
